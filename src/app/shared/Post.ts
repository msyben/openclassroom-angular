
export class Post {
  loveIts: number;
  createdAt: Date;

  constructor(public title: string, public content: string) {
    this.loveIts = 0;
    this.createdAt = new Date();
  }

  increment() {
    this.loveIts += 1;
  }
  decrement() {
    this.loveIts -= 1;
  }
}
