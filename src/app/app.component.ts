import { Component } from '@angular/core';
import { Post } from './shared/Post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  posts = [
    new Post('Mon premier post',
             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut in varius odio. Etiam ut nibh vulputate tortor tempus eleifend eu sit amet metus. Integer vitae ante sem. Aliquam sapien ante.'
    ),
    new Post('Mon deuxième post',
             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut in varius odio. Etiam ut nibh vulputate tortor tempus eleifend eu sit amet metus. Integer vitae ante sem. Aliquam sapien ante.'
    ),
    new Post('Encore un post',
             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut in varius odio. Etiam ut nibh vulputate tortor tempus eleifend eu sit amet metus. Integer vitae ante sem. Aliquam sapien ante.'
    )
  ];
}

